import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CheckComponent } from './check/check.component';
import { FabComponent } from './fab/fab.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireFunctionsModule, FUNCTIONS_ORIGIN } from '@angular/fire/functions';

let providers = [
  !environment.production && {
    provide: FUNCTIONS_ORIGIN,
    useValue: 'http://localhost:5001'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CheckComponent,
    FabComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    HttpClientModule,
  ],
  providers: providers.filter(Boolean),
  bootstrap: [AppComponent]
})
export class AppModule { }
