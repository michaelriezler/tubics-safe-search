import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize, switchMap, last } from 'rxjs/operators';
import nanoid from 'nanoid'
import { Observable } from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { RpcService } from '../rpc.service';

@Component({
  selector: 'app-check',
  templateUrl: './check.component.html',
  styleUrls: ['./check.component.css']
})
export class CheckComponent implements OnInit {

  progress: Observable<number>;
  thumbnail: string | ArrayBuffer;

  states = {
    NOT_STARTED: 'NOT_STARTED',
    STARTED: 'STARTED',
    RESULT: 'RESULT',
    ERROR: 'ERROR',
  };

  currentState = {
    state: this.states.NOT_STARTED,
    value: null,
  };

  constructor(
    private storage: AngularFireStorage,
    private rpc: RpcService,
    private db: AngularFirestore,
  ) { }

  ngOnInit() {
  }

  async upload(event) {
    let file = event.target.files[0];
    this.generateThumbnail(file).then(url => this.thumbnail = url)

    let img = await this.rpc
      .resizeImage(file)
      .then((result: Uint8Array) => new Blob([result], { type: file.type }));
    
    let filePath = nanoid();
    let fileRef = this.storage.ref(filePath);
    let task = this.storage.upload(filePath, img);

    this.progress = task.percentageChanges();
    
    task
      .snapshotChanges()
      .pipe(
        last(),
        switchMap(() => {
          this.setState(this.states.STARTED);
          return this.rpc.safeSearch(filePath);
        }),
        switchMap((result) => {
          this.setState(this.states.RESULT, result);
          return this.db
            .collection('results')
            .doc(filePath)
            .set({ file: file.name, result });
        })
      )
      .subscribe({
        error: error => this.setState(this.states.ERROR, error),
      })


  }

  setProgress(progress) {
    let percent = 100 - progress;
    return {
      transform: `translateX(-${percent}%)`
    }
  }

  generateThumbnail(file): Promise<string | ArrayBuffer> {
    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.onloadend = function() {
        resolve(reader.result);
      }
      reader.readAsDataURL(file);
    })
  }

  getImageDimension(file): Promise<{ width: number, height: number }> {
    return new Promise((resolve, reject) => {
      let fr = new FileReader();
      fr.onload = function() {
          let img = new Image();
          img.onload = () => resolve({ width: img.width, height: img.height })
          img.src = fr.result as string;
      };
      fr.readAsDataURL(file);
    }) 
  }

  setState(nextState, value = null) {
    this.currentState = {
      state: nextState,
      value: value,
    }
  }

  reset() {
    this.progress = undefined;
    this.thumbnail = undefined;
    this.currentState = {
      state: this.states.NOT_STARTED,
      value: null,
    }
  }

}
