import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, forkJoin } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { map, flatMap } from 'rxjs/operators'

interface Item {
  id: string;
  name: string;
  result: {
    adult: string;
    medical: string;
    spoof: string;
    violence: string;
    racy: string;
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  images: string[] = [];

  constructor(
    private db: AngularFirestore,
    private storage: AngularFireStorage,
  ) {
    this.itemsCollection = db.collection<Item>('results');
    this.items = this.itemsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        let data = a.payload.doc.data() as Item;
        let id = a.payload.doc.id;
        return { id, ...data };
      }))
    );

    this.items.pipe(
      flatMap(items => {
        let xs = items.map(x => {
          let id = x.id;
          return this.storage.ref(id).getDownloadURL();
        });

        return forkJoin(xs)
      }),
    )
    .subscribe(urls => this.images = urls)
  }

  ngOnInit() {
  }

}
