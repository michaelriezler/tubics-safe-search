/// <reference lib="webworker" />

import { Message }from './types';
import Jimp from 'jimp';

declare function postMessage(message: any, transfer?: ArrayBuffer[]): void;

interface Msg {
  id: string;
  type: Message;
  payload: any;
}

class Router {
  private fns: Map<Message, (any) => any> = new Map();

  constructor() {
    this.onMessage = this.onMessage.bind(this);
  }

  use(msg: Message, fn: (any) => any): Router {
    this.fns.set(msg, fn);
    return this
  }

  async onMessage(e): Promise<void> {
    let data = e.data as Msg;
    let fn = this.fns.get(data.type);

    if (!fn) {
      return postMessage({
        id: data.id,
        error: JSON.stringify({ msg: `Mismatched type: Can't find function ${data.type}` }),
      });
    }

    try {
      let result = await Promise.resolve(fn(data.payload));
      let transfer = [];
      
      if (result instanceof ArrayBuffer) {
        transfer.push(result);
      }

      postMessage({ id: data.id, payload: result }, transfer);
    } catch (error) {
      console.error(error);
      postMessage({ id: data.id, error: JSON.stringify(error) });
    }
  }

  start(): void  {
    self.addEventListener('message', this.onMessage);
  }
}

let router = new Router()

router.use(Message.ResizeImage, async ({ buffer, type }) => {
  let image = await Jimp.read(buffer);
  let result = await image.resize(500, Jimp.AUTO);
  return result.getBufferAsync(type);
});

router.start();
console.log('Worker is running');
