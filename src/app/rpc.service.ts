import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/functions';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs'
import nanoid from 'nanoid'
import { Message } from './types'

interface Resolver {
  resolve: (any) => void;
  reject: (any) => void;
}

@Injectable({
  providedIn: 'root'
})
export class RpcService {

  _safeSearch: (data: string) => Observable<any>;
  private resolvers: Map<string, Resolver> = new Map();
  private worker: Worker;

  constructor(private fns: AngularFireFunctions) {
    this._safeSearch = fns.httpsCallable('safeSearch');

    this.worker = new Worker('./app.worker', { type: 'module' });
    this.worker.onmessage = ({ data }) => {
      let { id, error, payload } = data;
      let fns = this.resolvers.get(id);

      if (error) {
        fns.reject(JSON.parse(error));
      } else {
        fns.resolve(payload);
      }

      this.resolvers.delete(id);
    };
  }

  safeSearch(fileName: string) {
    return this._safeSearch(fileName);
  }

  resizeImage(file: File) {
    return new Promise(async (resolve, reject) => {
      let id = nanoid();
      let buffer = await (new Response(file).arrayBuffer());
      let payload = { buffer, type: file.type };
      let msg = { id, type: Message.ResizeImage, payload };
      this.worker.postMessage(msg, [buffer]);
      this.resolvers.set(id, { resolve, reject });
    })
  }
}
