import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component'
import { CheckComponent } from './check/check.component'

const routes: Routes = [{
  path: '',
  component: HomeComponent,
}, {
  path: 'check',
  component: CheckComponent,
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
