if (process.env.FUNCTIONS_EMULATOR) {
  require('dotenv').config()
}

let functions = require('firebase-functions')
let vision = require('@google-cloud/vision')
let client = new vision.ImageAnnotatorClient()

const BUCKET_NAME = 'tubics.appspot.com'

let safeSearch = functions.https.onCall(async (fileName, ctx) => {
  let [result] = await client.safeSearchDetection(`gs://${BUCKET_NAME}/${fileName}`)
  return result.safeSearchAnnotation
})

module.exports = {
  safeSearch,
}
